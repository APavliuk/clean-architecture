//
//  ViewController.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/5/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PostsViewController: UIViewController {
        
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionAddNewPost: UIButton!
    
    var viewModel: PostsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        bindViewModel()
    }
    
    private func configTableView() {
        tableView.refreshControl = UIRefreshControl()
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func bindViewModel() {
        assert(viewModel != nil)
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        
        let input = PostsViewModel.Input(trigger: Driver.merge(viewWillAppear, pull),
                                         createPostTrigger: actionAddNewPost.rx.tap.asDriver(),
                                         selection: tableView.rx.itemSelected.asDriver())
        let output = viewModel.transform(input: input)
        
        output.posts.drive(tableView.rx.items(cellIdentifier: PostTableViewCell.reuseID, cellType: PostTableViewCell.self)) { (tv, model, cell) in
            cell.bind(model)
        }.disposed(by: disposeBag)
        
        output.selectedPost.drive()
            .disposed(by: disposeBag)
        output.createPost.drive()
            .disposed(by: disposeBag)
        output.fetching.drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        
    }
}

