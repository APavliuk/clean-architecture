import UIKit

final class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    func bind(_ viewModel: Post) {
        self.titleLabel.text = viewModel.title
        self.detailsLabel.text = viewModel.body
    }
    
}
